import React from "react";

import UsersList from "../components/UsersList";

const Users = () => {
  const USERS = [
    {
    id: 'u1',
    name: 'Neville Goddard',
    image: 'https://images.pexels.com/photos/14513736/pexels-photo-14513736.jpeg?w=200&h=200&fit=crop&dpr=1',
    places: 3,
    }
  ];

  return (
    <UsersList items={USERS} />
  );
};

export default Users;