import React from "react";
import { useParams } from "react-router-dom";

import PlaceList from "../components/PlaceList";

const PLACES = [
  {
    id: "p1",
    title: "Zions National Park",
    description:
      "Utah National Park with sky-scraper-like cliffs and switch back hiking",
    imageUrl:
      "https://www.zionponderosa.com/wp-content/uploads/2014/05/joshua-gresham-qxMqvmz4LC8-unsplash.jpg",
    address: "Angels Landing Trail, Hurricane, UT 84737",
    location: {
      lat: 37.2690649,
      lng: -112.9644084,
    },
    creator: "u1",
  },
  {
    id: "p2",
    title: "The Wave",
    description: "Wavy Red Rock Structures",
    imageUrl:
      "https://www.thewave.info/CoyoteButtesNorthCode/Images/ClassicShots/Large/The%20Classic%20Wave1000.jpg",
    address: "The Wave Trail, Kanab, AZ 84741",
    location: {
      lat: 36.9959312,
      lng: -112.0106366,
    },
    creator: "u2",
  },
];

const UserPlaces = (props) => {
  const userId = useParams().userId;
  const loadedPlaces = PLACES.filter((place) => place.creator === userId);

  return <PlaceList items={loadedPlaces} />;
};

export default UserPlaces;
