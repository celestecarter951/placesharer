import React, { useRef, useEffect } from "react";

import "./Maps.css";

const Map = (props) => {
  const { center, zoom } = props;
  const mapRef = useRef();

  useEffect(() => {
    const map = new window.google.maps.Map(mapRef.current, {
      center: center,
      zoom: zoom,
    });

    new window.google.maps.Marker({ position: center, map: map });
  }, [center, zoom]);

  return (
    <div
      ref={mapRef}
      className={`map ${props.className}`}
      style={props.style}
    ></div>
  );
};

// AIzaSyDPUac5h2G9yPeEzHoAFqDIxtsEhL9I0Jo

export default Map;
