const express = require("express");
const { body } = require("express-validator");

const usersController = require("../controllers/users-controller");

const router = express.Router();

router.get("/", usersController.getUsers);

router.post(
  "/signup",
  [
    body("email").normalizeEmail().isEmail(),
    body("name").notEmpty(),
    body("password").isLength({ min: 5 }),
  ],
  usersController.signup
);

router.post(
  "/login",
  [body("email").isEmail(), body("password").notEmpty()],
  usersController.login
);

module.exports = router;
