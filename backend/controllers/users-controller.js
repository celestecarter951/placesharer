const { v4: uuid } = require("uuid");
const HttpError = require("../models/http-error");
const { validationResult } = require("express-validator");

let USERS = [
  {
    id: "u1",
    name: "Harry Potter",
    email: "harrypotter@hogwarts.com",
    password: "shouldbeencypted",
  },
  {
    id: "u2",
    name: "Ron Weasley",
    email: "ronweasley@hogwarts.com",
    password: "notassecureasharryspassword",
  },
];

const getUsers = (req, res, next) => {
  console.log("get users", USERS);
  res.status(200).json({ users: USERS });
};

const signup = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new HttpError(
      "Something went wrong with your input, please check your inputs and try again.",
      422
    );
  }

  const { name, email, password } = req.body;

  // check if user already exists
  const hasUser = USERS.find((user) => user.email === email);
  if (hasUser) {
    throw new HttpError("Could not create user, email already exists.", 422);
  }

  const newUser = {
    id: uuid(),
    name,
    email,
    password,
  };

  USERS.push(newUser);

  res.status(201).json({ user: newUser });
};

const login = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new HttpError(
      "Something went wrong with your input, please check your inputs and try again.",
      422
    );
  }

  // check username and password
  const { email, password } = req.body;

  const loginUser = USERS.find((user) => user.email === email);

  if (!loginUser || loginUser.password !== password) {
    throw new HttpError(
      "Could not identify user, credentials seem to be wrong"
    );
    res.status(401);
  }

  res.status(200).json({ user: loginUser });
};

exports.getUsers = getUsers;
exports.signup = signup;
exports.login = login;
