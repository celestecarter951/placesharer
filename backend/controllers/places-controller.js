const { v4: uuid } = require("uuid");
const { validationResult } = require("express-validator");

const HttpError = require("../models/http-error");
const getCoordsForAddress = require("../util/location");
const { default: axios } = require("axios");

let PLACES = [
  {
    id: "p1",
    title: "Zions National Park",
    description:
      "Utah National Park with sky-scraper-like cliffs and switch back hiking",
    imageUrl:
      "https://www.zionponderosa.com/wp-content/uploads/2014/05/joshua-gresham-qxMqvmz4LC8-unsplash.jpg",
    address: "Angels Landing Trail, Hurricane, UT 84737",
    location: {
      lat: 37.2690649,
      lng: -112.9644084,
    },
    creator: "u1",
  },
  {
    id: "p2",
    title: "The Wave",
    description: "Wavy Red Rock Structures",
    imageUrl:
      "https://www.thewave.info/CoyoteButtesNorthCode/Images/ClassicShots/Large/The%20Classic%20Wave1000.jpg",
    address: "The Wave Trail, Kanab, AZ 84741",
    location: {
      lat: 36.9959312,
      lng: -112.0106366,
    },
    creator: "u2",
  },
];

const getPlacesByUserId = (req, res, next) => {
  const uid = req.params.uid;

  const places = PLACES.filter((place) => {
    return place.creator === uid;
  });

  if (!places || places.length === 0) {
    return next(
      new HttpError("Could not find a places for the provided user id", 404)
    );
  }

  res.json({ places });
};

const getPlaceById = (req, res, next) => {
  const placeId = req.params.pid;
  const place = PLACES.find((place) => {
    return place.id === placeId;
  });

  if (!place) {
    throw new HttpError("Could not find place for the provided place id", 404);
  }

  console.log("GET Request in Places");
  res.json({ place });
};

const createPlace = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }

  const { title, description, address, creator } = req.body;

  let coordinates;
  try {
    coordinates = await getCoordsForAddress(address);
  } catch (error) {
    return next(error);
  }

  const newPlace = {
    id: uuid(),
    title,
    description,
    location: coordinates,
    address,
    creator,
  };

  PLACES.push(newPlace);
  console.log("places now...", PLACES);

  res.status(201).json({ place: newPlace });
};

const updatePlace = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new HttpError("Invalid inputs passed, please check your data.", 422);
  }

  const { title, description, coordinates, address, imageUrl } = req.body;
  const placeId = req.params.pid;

  // get the place and update a temp before updating the whole thing
  const placeIndex = PLACES.findIndex((place) => place.id === placeId);
  const updatePlace = PLACES[placeIndex];
  updatePlace.title = title ? title : updatePlace.title;
  updatePlace.description = description ? description : updatePlace.description;
  //updatePlace.address = address ? address : updatePlace.address;
  //updatePlace.location = coordinates ? coordinates : updatePlace.location;
  // updatePlace.imageUrl = imageUrl ? imageUrl : updatePlace.imageUrl;
  PLACES[placeIndex] = updatePlace;

  res.status(200).json({ place: updatePlace });
};

const deletePlace = (req, res, next) => {
  const placeId = req.params.pid;

  const toDeleteIndex = PLACES.findIndex((place) => place.id === placeId);

  if (toDeleteIndex < 0) {
    throw new HttpError("Could not find a place for that id", 404);
  }

  PLACES.splice(toDeleteIndex, 1);

  res.status(200).json({ message: `Successfully deleted place ${placeId}` });
};

exports.getPlacesByUserId = getPlacesByUserId;
exports.getPlaceById = getPlaceById;
exports.createPlace = createPlace;
exports.updatePlace = updatePlace;
exports.deletePlace = deletePlace;
