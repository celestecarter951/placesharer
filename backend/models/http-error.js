class HttpError extends Error {
  constructor(message, errorCode) {
    super(message); // Adds an error message
    this.code = errorCode; // Adds a error code
  }
}

module.exports = HttpError;
